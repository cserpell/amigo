"""Secret friends email sender."""
import logging
import random
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


TEST_FRIENDS = {
    'cristian@serpell.cl': 'Cristi&aacute;n 1',
    'cserpell@gmail.com': 'Cristi&aacute;n 2',
}


def assign(friends):
    """Take persons email, person info dictionary and assign friends."""
    # Initialize random seed with system time
    random.seed()
    friends_list = list(friends.keys())
    assignees = {}
    total = len(friends)
    used = [False] * total
    orig = 0
    while orig < total:
        if orig == total - 1 and not used[orig]:
            # Start over
            used = [False] * total
            orig = 0
            continue
        dest = orig
        while dest == orig or used[dest]:
            dest = random.randint(0, total - 1)
        used[dest] = True
        assignees[friends_list[orig]] = friends_list[dest]
        orig += 1
    return assignees


def basic_text(html_body):
    """Remove some html tags from text to convert it to plain text."""
    test = html_body.replace('<br /><br />', '\n\n').replace('<b>', '')
    test = test.replace('</b>', '').replace('&iacute;', 'i')
    test = test.replace('&aacute;', 'a').replace('&eacute;', 'e')
    test = test.replace('&oacute;', 'o').replace('&uacute;', 'u')
    test = test.replace('&ntilde;', 'gn').replace('&iexcl;', '')
    return test


def send_email(orig, dest, title, body, smtp):
    """Create and send email using given smtp client."""
    msg = MIMEMultipart('alternative')
    msg['Subject'] = title
    msg['From'] = orig
    msg['To'] = dest
    text_part = MIMEText(basic_text(body), 'plain')
    html_part = MIMEText(body, 'html')
    msg.attach(text_part)
    msg.attach(html_part)
    logging.info('Sending email to %s', dest)
    smtp.sendmail(orig, dest, msg.as_string())


def run(friends):
    """Program that process friends dictionary and send emails."""
    smtp = smtplib.SMTP('localhost')
    assignments = assign(friends)
    for orig_email, dest_email in assignments.items():
        title = "IMPORTANTE: Amigo secreto 2018"
        body = '<br /><br />'.join([
            '&iexcl;Hola %s!' % friends[orig_email],
            'Espero que te encuentres bien. Se acerca el fin de a&ntilde;o, y '
            'con &eacute;l la navidad y el tan esperado amigo secreto.',
            '<img src="https://www.bosquedefantasias.com/wp-content/uploads/2018/01/cuento-infantil-navidad-en-la-ciudad.jpg" alt="Navidad en la ciudad" width="500" height="391"><br />'
            'Y ahora... '
            '&iexcl;Tu amigo secreto este a&ntilde;o es '
            '<b>%s</b>!' % friends[dest_email],
            'La cuota es de 15 mil pesos. Recuerda que la idea es '
            'divertirnos y que todos recibamos regalos, as&iacute; que no te '
            'estreses.',
            'Adem&aacute;s acu&eacute;rdate que este a&ntilde;o el regalo '
            'adicional ser&aacute; de dos lucas.',
            'Abrazos y nos vemos el 25 de diciembre,',
            'Cristi&aacute;n'])
        send_email('cristian@serpell.cl', orig_email, title, body, smtp)
    smtp.quit()


def main():
    """Main execution point."""
    run(TEST_FRIENDS)


if __name__ == '__main__':
    main()
