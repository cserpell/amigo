#!/usr/bin/awk -f

BEGIN {
 FS="\t"
 i = 0
}

{
 mail[i] = $2
 nomb[i] = $1
 i=i+1
}

END {
 srand()
 do
 {
  # Crea un arreglo de quien a quien, inizializado de cada uno a si mismo.
  for(j=0;j<i;j++)
   quien[j] = j
  # Aleatoriza, revolviendo el arreglo
  for(j=0;j<i;j++) {
   # Obtiene un lugar al azar
   k = int(i * rand())
   # Reemplaza el 'k' por el 'j'
   t = quien[k]
   quien[k] = quien[j]
   quien[j] = t
  }
  # Revisa que a nadie le toca a si mismo.
  for(j=0;j<i;j++)
  {
   if( quien[j] == j )
    break;
  }
 } while( j != i) # Si j == i, paso por todos ok.

 # Escribe los resultados
 for(j=0;j<i;j++)
 {
  mensaje = sprintf("\
¡Hola %s!\n\
\n\
En esta fiesta de fin de año, el Jueves 18 de Diciembre a las 17:00, te toca\n\
hacer un regalo para tu amigo secreto: %s.\n\
\n\
Este regalo debe ser de un valor entre $2500 y $3500.\n\
\n\
¡No faltes!\n\
\n\
Nota: este mensaje fue generado por un programa, nadie además de ti lo ha visto.\n", nomb[j], nomb[quien[j]])
  comando = sprintf("mail -s 'Amigo Secreto' '%s'", mail[j]);
  print mensaje | comando
 }
}

